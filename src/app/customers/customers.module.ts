import { CustomersRoutingModule } from './customers-routing.module';
import { FormsModule } from '@angular/forms';
import { FilterTextboxComponent } from './customers-list/filter-textbox.component';
import { CustomersListComponent } from './customers-list/customers-list.component';
import { CustomersComponent } from './customers.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [CustomersComponent, CustomersListComponent, FilterTextboxComponent],
  imports: [CommonModule, SharedModule, FormsModule, CustomersRoutingModule],
  exports: [CustomersComponent]
})
export class CustomersModule { }
