import { AppRoutingModule } from './app-routing-module';
import { SharedModule } from './shared/shared.module';
import { CustomersModule } from './customers/customers.module';
import { BrowserModule } from '@angular/platform-browser';

import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { OrdersComponent } from './orders/orders.component';
import { OrdersModule } from './orders/orders.module';

@NgModule({
   imports: [ BrowserModule, CustomersModule, SharedModule, CoreModule, AppRoutingModule, OrdersModule ],
   declarations: [
      AppComponent
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
